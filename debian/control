Source: iproute2
Section: net
Priority: optional
Maintainer: Proxmox Support Team <support@proxmox.com>
Homepage: https://wiki.linuxfoundation.org/networking/iproute2
Vcs-Browser: https://git.proxmox.com/?p=iproute2.git
Vcs-Git: git://git.proxmox.com/git/iproute2.git
Standards-Version: 3.9.8
Build-Depends: bison,
               cm-super-minimal,
               debhelper (>= 9),
               flex,
               iptables-dev,
               libatm1-dev,
               libdb-dev,
               libelf-dev,
               libmnl-dev,
               libselinux1-dev,
               linux-libc-dev,
               linuxdoc-tools,
               lynx | lynx-cur,
               pkg-config,
               sudo,
               texlive-fonts-recommended,
               texlive-latex-base,
               texlive-latex-extra,
               texlive-latex-recommended,
               zlib1g-dev,

Package: iproute2
Priority: important
Architecture: any
Provides: arpd
Conflicts: arpd, iproute (<< 20130000-1)
Replaces: iproute
Depends: ${misc:Depends}, ${shlibs:Depends}
Recommends: ${ipmods:Recommends}
Suggests: iproute2-doc
Multi-Arch: foreign
Description: networking and traffic control tools
 The iproute2 suite is a collection of utilities for networking and
 traffic control.
 .
 These tools communicate with the Linux kernel via the (rt)netlink
 interface, providing advanced features not available through the
 legacy net-tools commands 'ifconfig' and 'route'.

Package: iproute2-doc
Section: doc
Architecture: all
Conflicts: iproute-doc (<< 20130000-1)
Replaces: iproute-doc
Depends: ${misc:Depends}
Description: networking and traffic control tools - documentation
 The iproute2 suite is a collection of utilities for networking and
 traffic control.
 .
 This package contains the documentation for iproute.

