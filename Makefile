PACKAGE=iproute2
VER=4.15.0
PKGREL=5

SRCDIR=iproute2
BUILDDIR=${SRCDIR}.tmp

ARCH:=$(shell dpkg-architecture -qDEB_BUILD_ARCH)

MAIN_DEB=iproute2_${VERSION}-${PKGREL}_${ARCH}.deb
OTHER_DEBS=\
	iproute2-doc_${VERSION}-${PKGREL}_all.deb	\
	iproute2-dbgsym_${VERSION}-${PKGREL}_all.deb	\

DEBS=${MAIN_DEB} ${OTHER_DEBS}

DSC=${PACKAGE}_${VER}-${PKGREL}.dsc

all: ${DEBS}
	echo ${DEBS}

.PHONY: submodule
submodule:
	test -f "${SRCDIR}/README" || git submodule update --init

${BUILDDIR}: | submodule
	rm -rf $(BUILDDIR)
	cp -a $(SRCDIR) $(BUILDDIR)
	cp -a debian $(BUILDDIR)/debian

.PHONY: deb
deb: ${DEBS}
${OTHER_DEBS}: ${MAIN_DEBS}
${MAIN_DEB}: ${BUILDDIR}
	cd ${BUILDDIR}; dpkg-buildpackage -b -uc -us

.PHONY: dsc
dsc: ${DSC}
${DSC}: ${BUILDDIR}
	cd ${BUILDDIR}; debian/rules clean
	cd ${BUILDDIR}; tar czf ../${PACKAGE}_${VER}.orig.tar.gz *
	cd ${BUILDDIR}; dpkg-buildpackage -S -uc -us -d
	lintian $@

.PHONY: upload
upload: ${DEBS}
	tar cf - ${DEBS} | ssh repoman@repo.proxmox.com -- upload --product pve --dist stretch --arch ${ARCH}

.PHONY: clean distclean
distclean: clean
clean:
	rm -rf ${BUILDDIR} *.deb *.changes *.dsc *.buildinfo *.orig.tar.* *.debian.tar.*

.PHONY: dinstall
dinstall: deb
	dpkg -i ${DEB}
